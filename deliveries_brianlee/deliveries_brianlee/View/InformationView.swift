//
//  InformationView.swift
//  deliveries_brianlee
//
//  Created by Sing Ting on 2/6/2019.
//  Copyright © 2019 BrianLee. All rights reserved.
//

import UIKit

class InformationView: UIView {
    var imageView = UIImageView()
    var descriptionLabel = UILabel()
    
    override func draw(_ rect: CGRect) {
        self.backgroundColor = .white
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
        
        // Drawing code
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        self.addSubview(imageView)
        
        descriptionLabel.numberOfLines = 0
        self.addSubview(descriptionLabel)
        
        // Add auto layout constraints
        imageView.autoPinEdge(toSuperviewEdge: .top)
        imageView.autoPinEdge(toSuperviewEdge: .leading)
        imageView.autoPinEdge(toSuperviewEdge: .bottom)
        imageView.autoMatch(.width, to: .height, of: self)
        
        descriptionLabel.autoPinEdge(.leading, to: .trailing, of: imageView, withOffset: 8)
        descriptionLabel.autoPinEdge(toSuperviewMargin: .trailing)
        descriptionLabel.autoPinEdge(toSuperviewEdge: .top)
        descriptionLabel.autoPinEdge(toSuperviewEdge: .bottom)
    }
    
    func setupInformation(deliveryItem: DeliveryModel?) {
        guard let _deliveryItem = deliveryItem else {
            return
        }
        descriptionLabel.text = _deliveryItem.description ?? ""
        // Download image
        if let validURL = URL(string: _deliveryItem.imageUrl ?? "") {
            imageView.af_setImage(withURL: validURL, placeholderImage: UIImage.init(named: "placeholder"))
        }
    }
}
