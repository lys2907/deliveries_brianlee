//
//  DeliveryResponseModel.swift
//  deliveries_brianlee
//
//  Created by Sing Ting on 1/6/2019.
//  Copyright © 2019 BrianLee. All rights reserved.
//

import UIKit


struct DeliveryModel: Codable {
    let id: Int
    let description: String?
    let imageUrl: String?
    let location: location?

    struct location: Codable {
        let lat: Double?
        let lng: Double?
        let address: String?
    }
}

