//
//  DeliveryRequestModel.swift
//  deliveries_brianlee
//
//  Created by Sing Ting on 1/6/2019.
//  Copyright © 2019 BrianLee. All rights reserved.
//

import UIKit

struct DeliveryRequestModel: Codable {
    var limit: Int?
    var offset: Int?
    var paramter: [String: Any]? {
        do {
            if let dictionary = try JSONSerialization.jsonObject(with: JSONEncoder().encode(self), options: []) as? [String: Any] {
                return dictionary
            }
        } catch {
            return nil
        }
        return nil
    }
}
