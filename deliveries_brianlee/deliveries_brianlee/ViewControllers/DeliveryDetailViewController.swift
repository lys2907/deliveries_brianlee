//
//  DeliveryDetailViewController.swift
//  deliveries_brianlee
//
//  Created by Sing Ting on 1/6/2019.
//  Copyright © 2019 BrianLee. All rights reserved.
//

import UIKit
import MapKit
import PureLayout
import AlamofireImage
class DeliveryDetailViewController: UIViewController{
    
    var deliveryItem: DeliveryModel?
    var mapView = MKMapView()
    var informationView = InformationView()
    
    convenience init(deliveryItem: DeliveryModel) {
        self.init()
        self.deliveryItem = deliveryItem
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.setupUI()
    }
    
    func setupUI() {
        self.title = "Delivery Details"
        self.view.backgroundColor = .white
        // Setup map
        self.setupMapLocation()
        self.view.addSubview(mapView)
        
        // Setup Information View
        informationView.setupInformation(deliveryItem: deliveryItem)
        self.view.addSubview(informationView)
        
        // Add auto layout constraints
        mapView.autoPinEdge(toSuperviewEdge: .top)
        mapView.autoPinEdge(toSuperviewEdge: .leading)
        mapView.autoPinEdge(toSuperviewEdge: .trailing)
        mapView.autoMatch(.width, to: .height, of: mapView)
        
        informationView.autoPinEdge(.top, to: .bottom, of: mapView, withOffset: 10)
        informationView.autoPinEdge(toSuperviewMargin: .leading)
        informationView.autoPinEdge(toSuperviewMargin: .trailing)
        informationView.autoSetDimension(.height, toSize: 80)
    }
}

// Map
extension DeliveryDetailViewController {
    func setupMapLocation() {
        guard let lat = deliveryItem?.location?.lat, let lng = deliveryItem?.location?.lng  else {
            return
        }
        let pin = MKPointAnnotation()
        pin.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        mapView.addAnnotation(pin)
        let mapCenter = CLLocationCoordinate2DMake(lat, lng)
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: mapCenter, span: span)
        mapView.region = region
    }
}
