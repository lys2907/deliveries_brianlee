//
//  DeliveryListViewController.swift
//  deliveries_brianlee
//
//  Created by Sing Ting on 1/6/2019.
//  Copyright © 2019 BrianLee. All rights reserved.
//

import UIKit
import PureLayout
import AlamofireImage

class DeliveryListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var deliveriesTableView = UITableView()
    private var activityIndicator = UIActivityIndicatorView()
    var deliveriesList: [DeliveryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.fetchDeliveryList()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        self.title = "Things to Deliver"
        self.view.backgroundColor = .white
        
        // Setup TableView
        deliveriesTableView.delegate = self
        deliveriesTableView.dataSource = self
        deliveriesTableView.register(ListTableViewCell.self, forCellReuseIdentifier: "listCell")
        deliveriesTableView.separatorStyle = .none
        self.view.addSubview(deliveriesTableView)
        
        //Setup activityIndicator
        activityIndicator.color = .black
        self.view.addSubview(activityIndicator)
        
        // Add auto layout constraints
        activityIndicator.autoCenterInSuperview()
        deliveriesTableView.autoPinEdgesToSuperviewEdges()
    }
    
    func fetchDeliveryList() {
        activityIndicator.startAnimating()
        
        // Load list from userdefaults
        if let cachedList = DeliveryAPIManager.shared.loadCachedList() {
            self.deliveriesList = cachedList
            self.updateUI()
        } else {
            // Load list from API
            let requestModel = DeliveryRequestModel(limit: 20, offset: 0)
            DeliveryAPIManager.shared.callAPI(requestMode: requestModel, success: { (deliveriesList) in
                DeliveryAPIManager.shared.saveCacheList(deliveriesList)
                self.deliveriesList = deliveriesList
                self.updateUI()
            }) { (error) in
                self.activityIndicator.stopAnimating()
            }
        }
        
    }
    
    func updateUI() {
        self.deliveriesTableView.reloadData()
        self.activityIndicator.stopAnimating()
    }
}

class ListTableViewCell: UITableViewCell {
    var informationView = InformationView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //Do your cell set up
        contentView.addSubview(informationView)
        informationView.autoPinEdgesToSuperviewMargins()
        informationView.autoSetDimension(.height, toSize: 80)
    }
}

//MARK: UITableView Delegate Methods
extension DeliveryListViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliveriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! ListTableViewCell
        let deliveryItem = deliveriesList[indexPath.row]
        cell.informationView.setupInformation(deliveryItem: deliveryItem)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let deliveryItem = deliveriesList[indexPath.row]
        let viewController = DeliveryDetailViewController.init(deliveryItem: deliveryItem)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

