//
//  DeliveryAPIManager.swift
//  deliveries_brianlee
//
//  Created by Sing Ting on 1/6/2019.
//  Copyright © 2019 BrianLee. All rights reserved.
//

import UIKit
import Alamofire
class DeliveryAPIManager: NSObject {
    static let shared = DeliveryAPIManager()
    
    struct apiConstant {
        static let domain = "https://mock-api-mobile.dev.lalamove.com"
        static let endPoint = "/deliveries"
        static let APIPath = apiConstant.domain + apiConstant.endPoint
    }
    
    func callAPI(requestMode: DeliveryRequestModel?, success: @escaping ([DeliveryModel]) -> Void, failure: @escaping (Error?) -> Void) {
        Alamofire.request(apiConstant.APIPath, method: .get, parameters: requestMode?.paramter).responseJSON { response in
            if let data = response.data {
                do {
                    success(try JSONDecoder().decode([DeliveryModel].self, from: data))
                } catch {
                    failure(response.error)
                }
            } else {
                failure(response.error)
            }
        }
    }
}

extension DeliveryAPIManager {

    func saveCacheList(_ array: [DeliveryModel]) {
        let data = array.map { try? JSONEncoder().encode($0) }
        UserDefaults.standard.set(data, forKey: "deliveriesKey")
    }
    
    func loadCachedList() -> [DeliveryModel]? {
        guard let encodedData = UserDefaults.standard.array(forKey: "deliveriesKey") as? [Data] else {
            return nil
        }
        return encodedData.map { try! JSONDecoder().decode(DeliveryModel.self, from: $0) }
    }
}
